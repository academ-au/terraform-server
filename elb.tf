# module "server_elb" {
#   source = "terraform-aws-modules/elb/aws"

#   name = "${var.name}-elb"

#   subnets         = "${var.subnets}"
#   security_groups = "${var.security_groups}"
#   internal        = false

#   listener = [
#     {
#       instance_port     = "80"
#       instance_protocol = "HTTP"
#       lb_port           = "80"
#       lb_protocol       = "HTTP"
#     },
#   ]

#   health_check = [
#     {
#       target              = "TCP:80"
#       interval            = 10
#       healthy_threshold   = 2
#       unhealthy_threshold = 2
#       timeout             = 5
#     }
#   ]

#   tags 					      = "${var.tags}"
# }

# resource "aws_elb" "server" {
#   name            = "${var.name}-elb"
#   subnets         = var.subnets
#   security_groups = concat(var.security_groups,[aws_security_group.server_ipwhitelist_sg.id])

#   listener {
#     instance_port     = 80
#     instance_protocol = "http"
#     lb_port           = 80
#     lb_protocol       = "http"
#   }

#   listener {
#     instance_port      = 80
#     instance_protocol  = "http"
#     lb_port            = 443
#     lb_protocol        = "https"
#     ssl_certificate_id = module.aws_acm_certificate.acm_certificate_arn
#   }

#   health_check {
#     healthy_threshold   = 2
#     unhealthy_threshold = 2
#     timeout             = 3
#     target              = "TCP:80"
#     interval            = 10
#   }

#   cross_zone_load_balancing   = true
#   idle_timeout                = 400
#   connection_draining         = true
#   connection_draining_timeout = 400

#   tags = merge(var.tags,var.tags_anchor)
  
#   depends_on = [
#       // aws_route53_record.server_stg-review-edu_com-A,
#       // aws_route53_record.server-api_stg-review-edu_com-A,
#       module.aws_acm_certificate
#     ]
# }

