resource "aws_s3_bucket" "server_fs_bucket" {
  count  = (var.s3_enabled && var.s3_create) ? 1 : 0
  bucket = "${var.name}-fs"
  acl    = "private"
  tags   = var.tags

  force_destroy = var.s3_can_destroy //TODO: Look into if prevent_destroy is actually blocking this.

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  # logging {
  #   target_bucket = var.db_backups_bucket
  #   target_prefix = "review-stg-db-backup-"
  # }
}