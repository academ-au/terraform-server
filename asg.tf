module "server_asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 6.2"

  name    = "${var.name}-asg"

  # Launch configuration
  launch_template_name = "${var.name}-server-lc"
  update_default_version = true

  image_id          = data.aws_ami.server.image_id
  instance_type     = var.instance_type
  key_name          = var.key_name
  enable_monitoring = true

  create_iam_instance_profile = false
  iam_instance_profile_arn  = aws_iam_instance_profile.server_profile.arn
  security_groups = concat(var.security_groups,[aws_security_group.server_ipwhitelist_sg.id])
  user_data       = base64encode(data.template_file.initialize.rendered)

  block_device_mappings = [
  {
    # Root volume
    device_name = "/dev/xvda"
    no_device   = 0
    ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = var.volume_size
        volume_type           = "gp2"
      }
    }
  ]

  # Auto scaling group
  vpc_zone_identifier           = var.subnets
  health_check_type             = "ELB"
  min_size                      = var.asg_min
  max_size                      = var.asg_max
  desired_capacity              = var.asg_desired
  wait_for_capacity_timeout     = 0
  health_check_grace_period     = 300
  target_group_arns             = [aws_lb_target_group.target-group-80.arn]
  tags                          = merge(
    var.tags,
    {
      "cron" = join(" & ",var.cron)
      "environment_name" = "${var.environment}.${var.short_name}"
      "database" = "${var.short_name}.${var.environment}"
      "app_name" = var.app_name
      "app_version" = var.app_version
      "php_version" = var.php_version
      INTCNMonitored = true
      INTCustomer = "ACD"
      INTService = "ESS"
      INTCWAgent = true
    }
  )

  instance_refresh = {
    strategy = "Rolling"
    preferences = {
      checkpoint_delay       = 600
      checkpoint_percentages = [35, 70, 100]
      instance_warmup        = 600
      min_healthy_percentage = 90
    }
    triggers = ["tag"]
  }
}

resource "aws_autoscaling_policy" "server_scale_up" {
  name = "${var.name}-server-scale-up"
  policy_type = "SimpleScaling"
  scaling_adjustment = 2
  adjustment_type = "ChangeInCapacity"
  cooldown = 60
  autoscaling_group_name = module.server_asg.autoscaling_group_name
}

resource "aws_autoscaling_policy" "server_scale_down" {
  name = "${var.name}-server-scale-down"
  policy_type = "SimpleScaling"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = module.server_asg.autoscaling_group_name
}

resource "aws_cloudwatch_metric_alarm" "server_asg_cpu_usage_is_very_high" {
  alarm_name = "${var.name}-asg-cpu-usage-is-very-high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = 1
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = 30
  statistic = "Average"
  threshold = 60
  dimensions = {
    AutoScalingGroupName = module.server_asg.autoscaling_group_name
  }
  alarm_description = "This metric monitors EC2 CPU utilization."
  alarm_actions = [aws_autoscaling_policy.server_scale_up.arn]
}

resource "aws_cloudwatch_metric_alarm" "asg_cpu_usage_is_very_low" {
  alarm_name = "${var.name}-asg-cpu-usage-is-very-low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = 1
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = 300
  statistic = "Average"
  threshold = 20
  dimensions = {
    AutoScalingGroupName = module.server_asg.autoscaling_group_name
  }
  alarm_description = "This metric monitors EC2 CPU utilization."
  alarm_actions = [aws_autoscaling_policy.server_scale_down.arn]
}

resource "aws_cloudwatch_metric_alarm" "unhealthy_hosts" {
  alarm_name          = "${var.name}-unhealthy-hosts"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "UnhealthyHostCount"
  namespace           = "AWS/ApplicationELB"
  period              = "60"
  statistic           = "SampleCount"
  threshold           = "1"
  alarm_description   = "This metric checks for unhealthy hosts in the target group"
  alarm_actions       = [aws_autoscaling_policy.server_scale_up.arn]
  dimensions = {
    TargetGroup = aws_lb_target_group.target-group-80.name
  }
}