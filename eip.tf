resource "aws_eip" "pool" {
  count    = var.num_eips
  vpc      = true
  tags	   = {
	  Name = var.name
	  environment = var.environment
  }
}