resource "aws_security_group" "server_ipwhitelist_sg" {
  name        = "${var.name}-ipwhitelist_sg"
  description = "Allow ip restricted inbound traffic."
  vpc_id = var.vpc_id
  tags   = var.tags

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks =  [
      for ipAddress in var.ip_whitelist:
        length(split("/", ipAddress)) > 1 ? ipAddress : "${ipAddress}/32"
    ]
  }
  
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks =  [
      for ipAddress in var.ip_whitelist:
        length(split("/", ipAddress)) > 1 ? ipAddress : "${ipAddress}/32"
    ]
  }
  depends_on = [aws_eip.pool]
}