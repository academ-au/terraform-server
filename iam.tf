resource "aws_iam_user" "server" {
	name = var.name
	path = "/${var.app_name}/"
	tags = var.tags
}

resource "aws_iam_access_key" "server" {
	user = aws_iam_user.server.name
}

resource "aws_iam_group" "server_group" {
  name = var.name
  path = "/${var.app_name}/"
}

resource "aws_iam_user_group_membership" "server_group_membership" {
  user = aws_iam_user.server.name

  groups = [
    aws_iam_group.server_group.name
  ]
}

resource "aws_iam_role" "server-role-ec2" {
  name = "${var.name}-role-ec2"

  # DO NOT FORMAT THIS!
  assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Action": "sts:AssumeRole",
			"Principal":
			{
				"Service": "ec2.amazonaws.com"
			},
			"Effect": "Allow",
			"Sid": ""
		}
	]
}
EOF
}

resource "aws_iam_role_policy_attachment" "dev-resources-ssm-policy" {
  role       = aws_iam_role.server-role-ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_group_policy" "policy_s3" {
  count  = (var.s3_enabled && var.s3_create) ? 1 : 0
  name		 = "${var.name}-policy-s3"
  group      = aws_iam_group.server_group.name

  # DO NOT FORMAT THIS!
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ListObjectsInBucket",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.server_fs_bucket[0].id}"
            ]
        },
        {
            "Sid": "AllObjectActions",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:GetObject",
                "s3:GetObjectAcl",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.server_fs_bucket[0].id}/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_user_policy_attachment" "server_user_policy_attachment_ec2" {
	user = aws_iam_user.server.name
	policy_arn = var.ec2_policy
}

resource "aws_iam_instance_profile" "server_profile" {
	name = "${var.name}-profile"
	role = aws_iam_role.server-role-ec2.name
}

resource "aws_iam_role_policy_attachment" "server-role-cloudwatch-logs-policy" {
  role       = aws_iam_role.server-role-ec2.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}

resource "aws_iam_group_policy" "cloudwatch_logs" {
  name		 = "${var.name}-cloudwatch-log"
  group      = aws_iam_group.server_group.name

  # DO NOT FORMAT THIS!
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:*"
      ],
      "Resource": "arn:aws:logs:*:*:*:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}