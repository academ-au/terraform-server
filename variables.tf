variable "short_name" {
  description = ""
  default = "!!UNSET!!"
}

variable "name" {
  description = ""
}

variable "aws_account_id" {
  description = ""
}

variable "num_eips" {
  type = number
  default = 0
}

variable "environment" {
  description = ""
  default = "!!NOT SET!!"
}

variable "app_name" {
  description = ""
  default = "review"
}

variable "app_version" {
  description = ""
}

variable "php_version" {
  description = ""
}

variable "subnets" {
  description = ""
  type        = list(string)
}

variable "security_groups" {
  description = ""
  type        = list(string)
}

variable "server_group_name" {
  description = ""
}

variable "instance_type" {
  description = ""
}

variable "key_name" {
  description = ""
}

variable "domain" {
  description = ""
}

variable "domain_api" {
  description = ""
  default = ""
}

variable "domain_local" {
  description = ""
}

variable "zone_id" {
  description = ""
}

variable "local_zone_id" {
  description = ""
}

variable "region" {
  description = ""
}

variable "vpc_id" {
  description = ""
}

variable "s3_policy" {
  description = ""
}

variable "ec2_policy" {
  description = ""
  default = ""
}

variable "asg_min" {
  type = number
  default = 1
}

variable "asg_max" {
  type = number
  default = 1
}

variable "asg_desired" {
  type = number
  default = 1
}

variable "ip_whitelist" {
  description = ""
  type = list(string)
  default = ["0.0.0.0/0"]
}

variable "cron" {
  description = ""
  type = list(string)
  default = []
}

variable "tags" {
  description = ""
  type        = map(string)
}

variable "tags_anchor" {
  description = ""
  type        = map(string)
}

variable "alarm_action_critical_error" {
  description = ""
  type        = string
  default     = "arn:aws:sns:ap-southeast-2:168729627698:server-error-5XX"
}

variable "alarm_action_heavy_server_load" {
  description = ""
  type        = string
  default     = "arn:aws:sns:ap-southeast-2:168729627698:heavy-server-load"
}

variable "s3_enabled" {
  description = ""
  type        = bool
  default     = true
}

variable "s3_create" {
  description = ""
  type        = bool
  default     = true
}

variable "s3_can_destroy" {
  description = ""
  type        = bool
  default     = false
}

variable "alarms_enabled" {
  description = ""
  type        = bool
  default     = true
}

variable "init_script" {
  description = ""
  default     = "initalize.sh"
}

variable "init_script_extra" {
  description = ""
  default     = ""
}

variable "db_root_username" {
  description = ""
  default     = ""
}

variable "db_root_password" {
  description = ""
  default     = ""
}

variable "db_role_password" {
  description = ""
  default     = ""
}

variable "docker_branch" {
  description = ""
  default     = "master"
}

# variable "db_backups_bucket" {
#   default     = ""
#   description = ""
# }

# variable "db_backups" {
#   description = ""
#   type        = map(string)
# }

variable "logzio_token" {
  description = "API Token from Logz.io"
  type        = string
  default     = ""
}

variable "nginx_auth" {
  description = "Nginx Basic Authentication Config"
  type        = string
  default     = ""
}

variable "profiler" {
  description = "Enable server profilling and debugging."
  type        = bool
  default     = false
}

variable "volume_size" {
  type = number
  default = 20
}

variable "alb" {
  description = ""
}

variable "efs_backup" {
  description = "Enabling EFS backup" 
  default = "DISABLED"
}
