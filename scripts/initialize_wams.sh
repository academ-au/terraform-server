#!/bin/bash

export PATH=$PATH:/snap/bin
export NGINX_AUTH='${nginx_auth}';
export UTILITY_SERVER_ADDRESS='${utility_address}';
export DOMAIN='${domain}';
export NGINX_AUTH='${nginx_auth}';

sudo echo "WAMS_NAME=\"${name}\"" >> /etc/environment
sudo echo "UTILITY_SERVER_ADDRESS=\"${utility_address}\"" >> /etc/environment
sudo echo "DOMAIN=\"${domain}\"" >> /etc/environment
sudo echo "NGINX_AUTH='${nginx_auth}'" >> /etc/environment

#reload environment
source /etc/environment

ENV="${environment}";

if [[ $ENV == "production" ]]; then
	DB_HOST="db.prd.wams.internal";
else
    DB_HOST="db.stg.wams.internal";
fi;

#Setup filebeat
sudo tee /etc/filebeat/filebeat.yml > /dev/null <<EOT
${filebeat_config}
EOT
sudo service filebeat restart

#Install And Configure AWS and the S3 File System
sudo tee /home/ubuntu/.passwd-s3fs > /dev/null <<EOT
${aws_access_key_id}:${aws_secret_access_key}
EOT

sudo tee /home/ubuntu/.aws/credentials > /dev/null <<EOT
[default]
aws_access_key_id=${aws_access_key_id}
aws_secret_access_key=${aws_secret_access_key}

EOT

mkdir /root/.aws
sudo tee /root/.aws/config > /dev/null <<EOT
[default]
region=ap-southeast-2
output=json

EOT
sudo tee /root/.aws/credentials > /dev/null <<EOT
[default]
aws_access_key_id=${aws_access_key_id}
aws_secret_access_key=${aws_secret_access_key}

EOT

sudo tee /etc/updatedb.conf > /dev/null <<EOT
PRUNE_BIND_MOUNTS="yes"
# PRUNENAMES=".git .bzr .hg .svn"
PRUNEPATHS="/tmp /var/spool /media /var/lib/os-prober /var/lib/ceph /home/.ecryptfs /var/lib/schroot /var/www/s3"
PRUNEFS="NFS nfs nfs4 rpc_pipefs afs binfmt_misc proc smbfs autofs iso9660 ncpfs coda devpts ftpfs devfs devtmpfs fuse.mfs shfs sysfs cifs lustre tmpfs usbfs udf fuse.glusterfs fuse.sshfs curlftpfs ceph fuse.ceph fuse.rozofs ecryptfs fusesmb s3fs"

EOT

mkdir -p /var/www/s3/fs
chown -R ubuntu:ubuntu /var/www/s3
chown -R ubuntu:ubuntu /home/ubuntu/.passwd-s3fs
chmod 600 /home/ubuntu/.passwd-s3fs
echo "s3fs is mounting bucket \"${bucket}\" to /var/www/s3/fs"
s3fs ${bucket} /var/www/s3/fs -o passwd_file=/home/ubuntu/.passwd-s3fs -o url="https://s3-${region}.amazonaws.com" -o allow_other -o allow_other,default_acl=public-read,umask=000,uid=0,gid=0
ln -s /var/www/s3/fs /var/www/wams-base/s3fs

#Write db config
echo "${_}{DB_HOST}:5432:*:${db_root_username}:${db_root_password}" >> /home/ubuntu/.pgpass
sudo chown ubuntu:ubuntu /home/ubuntu/.pgpass
sudo chmod 0600 /home/ubuntu/.pgpass

echo "${_}{DB_HOST}:5432:*:${db_root_username}:${db_root_password}" >> /root/.pgpass
sudo chown ubuntu:ubuntu /root/.pgpass
sudo chmod 0600 /root/.pgpass

DB_REF="${short_name}.${environment}"
SHORT_NAME=${short_name_upper}

echo "DB_REF is $DB_REF"

#Check if DB exists.
psql -lqt -h $DB_HOST -U master | cut -d \| -f 1 | grep -qw $DB_REF

if [[ $? -eq 1 ]]; then
	#If it does not, create it.
	echo "Setting up DB"
	createdb -h $DB_HOST -U master "$DB_REF"
	psql -h $DB_HOST -d $DB_REF -U master -c "CREATE ROLE \"$DB_REF\" WITH LOGIN"
	psql -h $DB_HOST -d $DB_REF -U master -c "ALTER ROLE \"$DB_REF\" WITH PASSWORD '${db_role_password}';"
	cd /var/www/wams-base/db
	./Create -H $DB_HOST -d $DB_REF -u $DB_REF
	sed -i "s/%ORG_SHORT%/$SHORT_NAME/g" minimal-instance-template.sql
	sed -i "s/%ORG_LONG%/$SHORT_NAME/g" minimal-instance-template.sql
	psql -h $DB_HOST -d $DB_REF -U master -f ./minimal-instance-template.sql
fi

cd /var/www/
#Copy customer config...
echo "Setting up config for customer..."
git clone --branch ${environment}/${short_name} --recurse-submodules git@bitbucket.org:academ-au/wams-configs.git
mkdir -p ./wams-base/src/include/config
sudo rm -R ./wams-configs/.git
sudo rm -R ./wams-base/src/include/config/*
cp -rT ./wams-configs ./wams-base/src/include/config
ln -s /var/www/wams-base/src/include/config /home/ubuntu/config
# touch shared/trunk/src/img/monash-logo.jpg      <-------Monash only, need to create symlink manually   Command: ln -s ~/public_html/shared/trunk/src/img/monash-logo.jpg ~/public_html/current/trunk/src/img/monash-logo.jpg
# touch shared/trunk/src/schedule/move-import    <-------Monash only, need to create symlink manually

if [[ ${useEIP} == 1 ]]; then
	echo "Attaching Elastic IP Address..."
	#Attach Elastic IP to this instance.
	# JSON=$(aws ec2 describe-addresses --filters Name=tag:Name,Values=${name},Name=tag:environment,Values=staging)
	n=0
	until [ $n -ge 300 ]
	do
		JSON=$(aws ec2 describe-addresses --filters Name=tag:Name,Values=${name})
		ALLOCATION_ID=$(echo $JSON | jq -r '[ .Addresses[] | select(.AssociationId==null) | select(.Tags[].Key=="environment") | select(.Tags[].Value=="${environment}") | select(.Tags[].Key=="Name") | select(.Tags[].Value=="${name}") | .AllocationId ][0]')
		INSTANCE_ID="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id || die \"wget instance-id has failed: $?\"`"

		if [[ $ALLOCATION_ID == null ]]; then
			echo "Failed. Retrying in 10 seconds..."
			n=$[$n+1] 
			sleep 10
		else
			SUCCESS=1
			echo "Attaching EIP with Allocation ID \"$ALLOCATION_ID\" to this instance ($INSTANCE_ID)"
			aws ec2 associate-address --instance-id $INSTANCE_ID --allocation-id $ALLOCATION_ID --allow-reassociation
			break
		fi
	done
else
	echo "EIP allocation not enabled."
	SUCCESS=1
fi

if [[ $SUCCESS == 0 ]]; then
	echo "Error: Failed to assign IP. Exiting..."
	exit
	# TODO: Log this!
fi

echo "Starting docker for WAMS..."
sudo chown -R ubuntu:ubuntu .
cd /var/www/wams-base
echo "./docker/docker-start prod"
bash ./docker/docker-start prod

${init_script_extra}

echo "Have a nice day :)"
