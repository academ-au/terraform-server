#!/bin/bash

export PATH=$PATH:/snap/bin
export REVIEW_NAME=${name}
export UTILITY_SERVER_ADDRESS=${utility_address}

sudo tee /etc/filebeat/filebeat.yml > /dev/null <<EOT
${filebeat_config}
EOT

sudo service filebeat restart

${init_script_extra}