#!/bin/sh

cat <<EOT > /home/ubuntu/.aws/credentials
[default]
aws_access_key_id=${aws_access_key_id}
aws_secret_access_key=${aws_secret_access_key}
region=ap-southeast-2
output=json

EOT