#!/bin/bash

export PATH=$PATH:/snap/bin
export REVIEW_NAME=${name}
export UTILITY_SERVER_ADDRESS=${utility_address}
export DOMAIN=${domain}

sudo echo "PATH=\"${_}{PATH}/snap/bin\"" >> /etc/environment
sudo echo "REVIEW_NAME=\"${name}\"" >> /etc/environment
sudo echo "UTILITY_SERVER_ADDRESS=\"${utility_address}\"" >> /etc/environment
sudo echo "DOMAIN=\"${domain}\"" >> /etc/environment

sudo apt-get install -q -y -f s3fs --allow-unauthenticated
echo "${aws_access_key_id}:${aws_secret_access_key}" >> /home/ubuntu/.passwd-s3fs
mkdir -p /var/www/s3/fs
sudo chown -R ubuntu:ubuntu /var/www/s3
sudo chown -R ubuntu:ubuntu /home/ubuntu/.passwd-s3fs
chmod 600 /home/ubuntu/.passwd-s3fs
sudo s3fs ${bucket} /var/www/s3/fs -o passwd_file=/home/ubuntu/.passwd-s3fs -o url="https://s3-${region}.amazonaws.com" -o allow_other -o allow_other,default_acl=public-read,umask=000,uid=0,gid=0

#Setup filebeat
sudo tee /etc/filebeat/filebeat.yml > /dev/null <<EOT
${filebeat_config}
EOT

sudo service filebeat restart

#Clone the correct review docker version according to the required PHP version.
cd /var/www
echo "Cloning review docker for PHP version ${php_version}"
git clone --progress --branch php${php_version} git@bitbucket.org:academ-au/review-docker.git
sudo rm ./review/docker/nginx/sites-enabled/*
cp -R review-docker/* review/

#Clone hotfixes.
git clone --progress --branch postfix-docker-localhost git@bitbucket.org:academ-au/review-hotfixes.git
cp -R review-hotfixes/* review/
#Install And Configure AWS and the S3 File System
# apt-get update
# apt-get install -q -y -f s3fs --allow-unauthenticated

mkdir /root/.aws
sudo tee /root/.aws/config > /dev/null <<EOT
[default]
region=ap-southeast-2
output=json

EOT
sudo tee /root/.aws/credentials > /dev/null <<EOT
[default]
aws_access_key_id=${aws_access_key_id}
aws_secret_access_key=${aws_secret_access_key}

EOT

sudo tee /etc/updatedb.conf > /dev/null <<EOT
PRUNE_BIND_MOUNTS="yes"
# PRUNENAMES=".git .bzr .hg .svn"
PRUNEPATHS="/tmp /var/spool /media /var/lib/os-prober /var/lib/ceph /home/.ecryptfs /var/lib/schroot /var/www/s3"
PRUNEFS="NFS nfs nfs4 rpc_pipefs afs binfmt_misc proc smbfs autofs iso9660 ncpfs coda devpts ftpfs devfs devtmpfs fuse.mfs shfs sysfs cifs lustre tmpfs usbfs udf fuse.glusterfs fuse.sshfs curlftpfs ceph fuse.ceph fuse.rozofs ecryptfs fusesmb s3fs"

EOT

# mkdir -p /var/www/s3/fs
# chown -R ubuntu:ubuntu /var/www/s3
# chown -R ubuntu:ubuntu /home/ubuntu/.passwd-s3fs
# chmod 600 /home/ubuntu/.passwd-s3fs
# echo "mounting S3 bucket"
# echo "s3fs is mounting bucket \"${bucket}\" to /var/www/s3/fs"
# s3fs ${bucket} /var/www/s3/fs -o passwd_file=/home/ubuntu/.passwd-s3fs -o url="https://s3-${region}.amazonaws.com" -o allow_other -o allow_other,default_acl=public-read,umask=000,uid=0,gid=0

SUCCESS=0

if [ ${useEIP} == 1 ]; then
	echo "Attaching Elastic IP Address..."
	#Attach Elastic IP to this instance.
	# JSON=$(aws ec2 describe-addresses --filters Name=tag:Name,Values=${name},Name=tag:environment,Values=staging)
	n=0
	until [ $n -ge 300 ]
	do
		JSON=$(aws ec2 describe-addresses --filters Name=tag:Name,Values=${name})
		ALLOCATION_ID=$(echo $JSON | jq -r '[ .Addresses[] | select(.AssociationId==null) | select(.Tags[].Key=="environment") | select(.Tags[].Value=="${environment}") | select(.Tags[].Key=="Name") | select(.Tags[].Value=="${name}") | .AllocationId ][0]')
		INSTANCE_ID="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id || die \"wget instance-id has failed: $?\"`"
		
		if [ $ALLOCATION_ID == null ]; then
			echo "Failed. Retrying in 10 seconds..."
			n=$[$n+1]
			sleep 10
		else
			SUCCESS=1
			echo "Attaching EIP with Allocation ID \"$ALLOCATION_ID\" to this instance ($INSTANCE_ID)"
			aws ec2 associate-address --instance-id $INSTANCE_ID --allocation-id $ALLOCATION_ID --allow-reassociation
			break
		fi
	done
	
else
	echo "EIP allocation not enabled."
	SUCCESS=1
fi

if [ $SUCCESS == 0 ]; then
	echo "Error: Failed to assign IP. Exiting..."
	exit
	# TODO: Log this!
fi

#Remove fixed folders.
echo "Removing some folders to make room for symlinks..."
rm -R /var/www/review/trunk/public/clientfiles
rm -R /var/www/review/trunk/clientfiles
rm -R /var/www/review/trunk/session
rm -R /var/www/review/trunk/application/configs/certificates

mkdir /var/logs/cron

#Replace with Symlinks
echo "Setting up symlinks..."
ln -sf /var/www/s3/fs/public/clientfiles /var/www/review/trunk/public/clientfiles
ln -sf /var/www/s3/fs/clientfiles /var/www/review/trunk/clientfiles
ln -sf /var/www/s3/fs/session /var/www/review/trunk/session
ln -sf /var/www/s3/fs/application/configs/application.ini /var/www/review/trunk/application/configs/application.ini
ln -sf /var/www/s3/fs/application/configs/certificates /var/www/review/trunk/application/configs/certificates

# #Clone the correct review docker version according to the required PHP version.
# cd /var/www
# echo "Cloning review docker for PHP version ${php_version}"
# git clone --progress --branch php${php_version} git@bitbucket.org:academ-au/review-docker.git
# cp -R review-docker/* review/

#Start docker (note that we start this before migration so that the auto scaler doesn't trigger this as a failed start due to timeout).
cd /var/www/review
echo "Pulling docker images..."

n=0
until [ $n -ge 10 ]
do
   docker compose pull && break
   echo "Failed. Retrying in 10 seconds..."
   n=$[$n+1]
   sleep 10
done

echo "Building docker images..."

n=0
until [ $n -ge 10 ]
do
   docker compose build && break
   echo "Failed. Retrying in 10 seconds..."
   n=$[$n+1]
   sleep 10
done

echo "Starting docker for Review..."
docker compose up -d


#Run the DB migration.
echo "Preparing to run DB migration..."
cd /var/www/review/services/db-migrator
cp /var/www/s3/fs/application/configs/db-migrator.env .env
FILE=/var/www/review/services/db-migrator/docker-compose.yml
if [ -f "$FILE" ]; then
	echo "Running DB migration..."
	git fetch
	git checkout origin/develop
	docker compose run php php artisan migrate
fi

${init_script_extra}
