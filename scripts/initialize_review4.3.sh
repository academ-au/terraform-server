#!/bin/bash

export PATH=$PATH:/snap/bin
export REVIEW_NAME=${name}
export UTILITY_SERVER_ADDRESS=${utility_address}
export DOMAIN=${domain}

sudo echo "PATH=\"${_}{PATH}/snap/bin\"" >> /etc/environment
sudo echo "REVIEW_NAME=\"${name}\"" >> /etc/environment
sudo echo "UTILITY_SERVER_ADDRESS=\"${utility_address}\"" >> /etc/environment
sudo echo "DOMAIN=\"${domain}\"" >> /etc/environment

#Setup filebeat
sudo tee /etc/filebeat/filebeat.yml > /dev/null <<EOT
${filebeat_config}
EOT

sudo service filebeat restart

cd /var/www
#Clone hotfixes.
git clone --progress --branch postfix-docker-localhost git@bitbucket.org:academ-au/review-hotfixes.git
cp -R review-hotfixes/* review/

mkdir /root/.aws
sudo tee /root/.aws/config > /dev/null <<EOT
[default]
region=ap-southeast-2
output=json

EOT
sudo tee /root/.aws/credentials > /dev/null <<EOT
[default]
aws_access_key_id=${aws_access_key_id}
aws_secret_access_key=${aws_secret_access_key}

EOT

mkdir -p /var/www/vfs/session
mkdir -p /var/www/vfs/fs
sudo chown ubuntu:ubuntu /var/www/vfs
sudo chown ubuntu:ubuntu /var/www/vfs/session
sudo chown ubuntu:ubuntu /var/www/vfs/fs

echo "VFS is mounting..."
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${efs_session_dns_name}:/ /var/www/vfs/session
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${efs_general_dns_name}:/ /var/www/vfs/fs
echo "VFS mounting complete!"

sudo rm -Rf /var/www/review/vfs
sudo rm -Rf /var/www/review/trunk/session
sudo ln -s /var/www/vfs /var/www/review/vfs
mkdir -p /var/www/vfs/fs/clientfiles/temp
chown -R ubuntu:ubuntu /var/www/vfs/fs/clientfiles/temp

cd /var/www/
#Copy customer config...
echo "Setting up config for customer..."
git clone --branch ${environment}/${short_name} git@bitbucket.org:academ-au/review-configs.git
mkdir -p ./review/application/configs
sudo rm -R ./review-configs/.git
cp -rT ./review-configs ./review/trunk/application/configs
sudo chown -R ubuntu:ubuntu ./review/trunk/application/configs
ln -s /var/www/review/trunk/application/configs /home/ubuntu/config

SUCCESS=0

if [ ${useEIP} == 1 ]; then
	echo "Attaching Elastic IP Address..."
	#Attach Elastic IP to this instance.
	# JSON=$(aws ec2 describe-addresses --filters Name=tag:Name,Values=${name},Name=tag:environment,Values=staging)
	n=0
	until [ $n -ge 300 ]
	do
		JSON=$(aws ec2 describe-addresses --filters Name=tag:Name,Values=${name})
		ALLOCATION_ID=$(echo $JSON | jq -r '[ .Addresses[] | select(.AssociationId==null) | select(.Tags[].Key=="environment") | select(.Tags[].Value=="${environment}") | select(.Tags[].Key=="Name") | select(.Tags[].Value=="${name}") | .AllocationId ][0]')
		INSTANCE_ID="`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id || die \"wget instance-id has failed: $?\"`"

		if [ $ALLOCATION_ID == null ]; then
			echo "Failed. Retrying in 10 seconds..."
			n=$[$n+1]
			sleep 10
		else
			SUCCESS=1
			echo "Attaching EIP with Allocation ID \"$ALLOCATION_ID\" to this instance ($INSTANCE_ID)"
			aws ec2 associate-address --instance-id $INSTANCE_ID --allocation-id $ALLOCATION_ID --allow-reassociation
			break
		fi
	done
else
	echo "EIP allocation not enabled."
	SUCCESS=1
fi

if [ $SUCCESS == 0 ]; then
	echo "Error: Failed to assign IP. Exiting..."
	exit
	# TODO: Log this!
fi

#Remove fixed folders.
echo "Removing some folders to make room for symlinks..."
rm -R /var/www/review/trunk/public/clientfiles
rm -R /var/www/review/trunk/clientfiles

mkdir /var/logs/cron

#Copy .env to graph
echo "Copying .env to graph"
cd /var/www/review/services/graph
cp /var/www/vfs/fs/application/configs/db-migrator.env .env

#Start docker (note that we start this before migration so that the auto scaler doesn't trigger this as a failed start due to timeout).
echo "Starting docker for Review..."
cd /var/www/review

#Run with the debug mode based on the profiler setting.
if [ ${useProfiler} == 1 ]; then
	export DEBUG_HEADER='fastcgi_param PHP_VALUE "auto_prepend_file=/var/www/xhgui/external/header.php";'
	sudo echo "DEBUG_HEADER=fastcgi_param PHP_VALUE \"auto_prepend_file=/var/www/xhgui/external/header.php\";" >> /etc/environment
	sudo cp /var/www/review/docker/config/xhprof/config.php /var/www/review/docker/data/xhgui/www/config/config.php
	sudo chown ubuntu:ubuntu /var/www/review/docker/data/xhgui/www/config/config.php
	sudo bash ./app start -e prod -d
else
	export DEBUG_HEADER=''
	sudo echo "DEBUG_HEADER=''" >> /etc/environment
	sudo bash ./app start -e prod
fi

#Run the DB migration.
echo "Preparing to run DB migration..."
cd /var/www/review/services/db-migrator
cp /var/www/review-configs/db-migrator.env .env
FILE=/var/www/review/services/db-migrator/docker-compose.yml
if [ -f "$FILE" ]; then
	echo "Running DB migration..."
	docker compose run php php artisan migrate
fi

${init_script_extra}
