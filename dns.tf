resource "aws_route53_record" "server_stg-review-edu_com-A" {
  zone_id = var.zone_id
  name    = var.domain
  type    = "A"

  alias {
    name                   = var.alb.dns_name
    zone_id                = var.alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "server-api_stg-review-edu_com-A" {
  count   = length(var.domain_api) > 0 ? 1 : 0
  zone_id = var.zone_id
  name    = var.domain_api
  type    = "A"

  alias {
    name                   = var.alb.dns_name
    zone_id                = var.alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "server-profiler_stg-review-edu_com-A" {
  count   = var.profiler ? 1 : 0
  zone_id = var.zone_id
  name    = "profiler.${var.domain}"
  type    = "A"

  alias {
    name                   = var.alb.dns_name
    zone_id                = var.alb.zone_id
    evaluate_target_health = true
  }
}

// resource "aws_route53_record" "server_review_local-CNAME" {
//   count   = var.num_eips
//   zone_id = var.local_zone_id
//   name    = "${count.index}.${var.short_name}.${var.environment}.review.local"
//   type    = "CNAME"
//   ttl     = "60"
//   records = ["${aws_eip.pool.*.private_ip}"]
// }