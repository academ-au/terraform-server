data "template_file" "filebeat_src" {
  template = file("${path.module}/templates/filebeat.yml.tmpl")

  vars = {
    logzio_token = var.logzio_token
    environment  = var.environment
    name         = var.name
  }
}


data "template_file" "initialize" {
  template = file("${path.module}/scripts/${var.init_script}")

  vars = {
    _                     = "$"
    useProfiler           = var.profiler ? 1 : 0
    init_script_extra     = var.init_script_extra
    aws_access_key_id     = aws_iam_access_key.server.id
    aws_secret_access_key = aws_iam_access_key.server.secret
    bucket                = "${var.name}-fs"
    filebeat_config       = data.template_file.filebeat_src.rendered
    name                  = var.name
    short_name            = var.short_name
    short_name_upper      = upper(var.short_name)
    domain                = var.domain
    php_version           = var.php_version
    region                = var.region
    useEIP                = var.num_eips > 0 ? 1 : 0
    environment           = var.environment
    logzio_token          = var.logzio_token
    db_root_username      = var.db_root_username
    db_root_password      = var.db_root_password
    db_role_password      = var.db_role_password
    utility_address       = (var.environment == "production") ? "utility.prd.${var.app_name}.internal" : "utility.${var.app_name}.internal"
    nginx_auth            = var.nginx_auth
    efs_general_dns_name  = aws_efs_mount_target.server_efs_general_1.dns_name
    efs_session_dns_name  = aws_efs_mount_target.server_efs_session_1.dns_name
  }
}
