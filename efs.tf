resource "aws_efs_file_system" "server_efs_general" {
  creation_token = "${var.name}-efs-general"
  performance_mode = "generalPurpose"
  encrypted = true

  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }

  tags = merge(var.tags,var.tags_anchor,{fs = "general"}, {
    INTCNMonitored = true,
    INTCustomer    = "ACD",
    INTService     = "ESS",
  })
}

resource "aws_efs_file_system" "server_efs_session" {
  creation_token = "${var.name}-efs-session"
  performance_mode = "maxIO"
  encrypted = true

  tags = merge(var.tags,var.tags_anchor,{fs = "session"}, {
    INTCNMonitored = true,
    INTCustomer    = "ACD",
    INTService     = "ESS",
  })
}

resource "aws_efs_backup_policy" "server_efs_general_backup_policy" {
  file_system_id = aws_efs_file_system.server_efs_general.id

  backup_policy {
    status = "${var.efs_backup}"
  }
}

# resource "aws_efs_backup_policy" "server_efs_session_backup_policy" {
#   file_system_id = aws_efs_file_system.server_efs_session.id

#   backup_policy {
#     status = "ENABLED"
#   }
# }

# resource "aws_efs_backup_policy" "policy" {
#   file_system_id = aws_efs_file_system.fs.id

#   backup_policy {
#     status = "ENABLED"
#   }
# }
resource "aws_efs_file_system_policy" "server_efs_general_policy" {
  file_system_id = aws_efs_file_system.server_efs_general.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "${var.name} EFS General Access",
    "Statement": [
        {
            "Sid": "Statement",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Resource": "${aws_efs_file_system.server_efs_general.arn}",
            "Action": [
                "elasticfilesystem:ClientMount",
                "elasticfilesystem:ClientRootAccess",
                "elasticfilesystem:ClientWrite"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        }
    ]
}
POLICY
}

resource "aws_efs_file_system_policy" "server_efs_policy" {
  file_system_id = aws_efs_file_system.server_efs_session.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "${var.name} EFS Session Access",
    "Statement": [
        {
            "Sid": "Statement",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Resource": "${aws_efs_file_system.server_efs_session.arn}",
            "Action": [
                "elasticfilesystem:ClientMount",
                "elasticfilesystem:ClientRootAccess",
                "elasticfilesystem:ClientWrite"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        }
    ]
}
POLICY
}

resource "aws_efs_mount_target" "server_efs_general_1" {
  file_system_id = aws_efs_file_system.server_efs_general.id
  subnet_id      = var.subnets[0]
}

resource "aws_efs_mount_target" "server_efs_general_2" {
  file_system_id = aws_efs_file_system.server_efs_general.id
  subnet_id      = var.subnets[1]
}

resource "aws_efs_mount_target" "server_efs_general_3" {
  file_system_id = aws_efs_file_system.server_efs_general.id
  subnet_id      = var.subnets[2]
}


resource "aws_efs_mount_target" "server_efs_session_1" {
  file_system_id = aws_efs_file_system.server_efs_session.id
  subnet_id      = var.subnets[0]
}

resource "aws_efs_mount_target" "server_efs_session_2" {
  file_system_id = aws_efs_file_system.server_efs_session.id
  subnet_id      = var.subnets[1]
}

resource "aws_efs_mount_target" "server_efs_session_3" {
  file_system_id = aws_efs_file_system.server_efs_session.id
  subnet_id      = var.subnets[2]
}

output "server_efs_general_dns_name" {
  value = aws_efs_mount_target.server_efs_general_1.dns_name
}

output "server_efs_session_dns_name" {
  value = aws_efs_mount_target.server_efs_session_1.dns_name
}


# resource "aws_security_group" "server_efs_general_target" {
#   name        = "${var.name}-efs_sg"
#   description = "General EFS Target SG."
#   vpc_id = var.vpc_id
#   tags   = var.tags
# }

# resource "aws_security_group" "server_efs_general_mount" {
#   name        = "${var.name}-efs_sg"
#   description = "General EFS Mount SG."
#   vpc_id = var.vpc_id
#   tags   = var.tags

#   ingress {
#     from_port   = 2049
#     to_port     = 2019
#     protocol    = "tcp"
#     cidr_blocks =  [
#       for ipAddress in var.ip_whitelist:
#         length(split("/", ipAddress)) > 1 ? ipAddress : "${ipAddress}/32"
#     ]
#   }
# }
# resource "aws_security_group" "server_efs_sg" {
#   name        = "${var.name}-efs_sg"
#   description = "Security Group for EFS."
#   vpc_id = var.vpc_id
#   tags   = var.tags

#   ingress {
#     from_port   = 2049
#     to_port     = 2019
#     protocol    = "tcp"
#     cidr_blocks =  [
#       for ipAddress in var.ip_whitelist:
#         length(split("/", ipAddress)) > 1 ? ipAddress : "${ipAddress}/32"
#     ]
#   }
# }
