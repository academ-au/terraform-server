module "metric_alarm_HTTPCode_ELB_5XX" {
  source = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  version = "~> 1.0"

  create_metric_alarm = var.alarms_enabled
  alarm_name          = "ELB 5XX - ${var.name}"
  alarm_description   = "Reports any 500 ranged HTTP errors on reported back to ELB."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  threshold           = 1
  period              = 60
  unit                = "Count"

  namespace   = "AWS/ELB"
  metric_name = "HTTPCode_ELB_5XX"
  statistic   = "Sum"

  alarm_actions = [var.alarm_action_critical_error]

  dimensions = {
    LoadBalancerName = "${var.name}-elb"
  }
}
module "metric_alarm_load" {
  source = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  version = "1.3.0"

  create_metric_alarm = var.alarms_enabled
  alarm_name          = "Server Load Warning - ${var.name}"
  alarm_description   = "Fires off an email when server is at high capacity."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  threshold           = 10
  period              = 60
  unit                = "Count"

  namespace   = "AWS/ELB"
  metric_name = "HTTPCode_Backend_5XX"
  statistic   = "Sum"

  alarm_actions = [var.alarm_action_heavy_server_load]

  dimensions = {
    LoadBalancerName = "${var.name}-elb"
  }
}

module "metric_alarm_HTTPCode_HTTP_5XX_ScaleUp" {
  source = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  version = "1.3.0"

  alarm_name          = "HTTP ELB 5XX Scale Up- ${var.name}"
  alarm_description   = "Autoscale up on HTTP ELB 5XX Errors."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  threshold           = 10
  period              = 60
  unit                = "Count"

  namespace   = "AWS/ELB"
  metric_name = "HTTPCode_Backend_5XX"
  statistic   = "Sum"

  alarm_actions = [aws_autoscaling_policy.server_scale_up.arn]

  dimensions = {
    LoadBalancerName = "${var.name}-elb"
  }
}

module "metric_alarm_HTTPCode_HTTP_5XX_ScaleDown" {
  source = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  version = "1.3.0"

  alarm_name          = "HTTP ELB 5XX Scale Down - ${var.name}"
  alarm_description   = "Autoscale down on HTTP ELB 5XX Errors."
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  threshold           = 1
  period              = 60
  unit                = "Count"

  namespace   = "AWS/ELB"
  metric_name = "HTTPCode_Backend_5XX"
  statistic   = "Sum"

  alarm_actions = [aws_autoscaling_policy.server_scale_down.arn]

  dimensions = {
    LoadBalancerName = "${var.name}-elb"
  }
}