resource "aws_lb_target_group" "target-group-80" {
  name     = "${var.name}-elb"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    path = "/hc.php"
    protocol = "HTTP"
    port = 80
    healthy_threshold = 6
    unhealthy_threshold = 2
    timeout = 2
    interval = 5
    matcher = "200-399"
  }
}