output "server_accessKeyId" {
  value = aws_iam_access_key.server.id
  sensitive = true
}

output "server_secret" {
  value = aws_iam_access_key.server.secret
  sensitive = true
}

output "elastic_ip_pool_ids" {
  value = aws_eip.pool.*.id
}

output "elastic_ip_pool_ips" {
  value = aws_eip.pool.*.public_ip
}

output "hostname" {
  value = var.domain
}

output "hostname_api" {
  value = var.domain_api
}

output "hostname_profiler" {
  value = aws_route53_record.server-profiler_stg-review-edu_com-A[*].fqdn
}

output "certificate" {
  value = module.aws_acm_certificate.acm_certificate_arn
}

output "target_group" {
  value = aws_lb_target_group.target-group-80.arn
}


# output "s3_bucket" {
#   value = aws_s3_bucket.server_fs_bucket.id
# }

