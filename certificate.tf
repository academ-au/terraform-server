module "aws_acm_certificate" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> v4.1"

  domain_name  = var.domain
  zone_id      = var.zone_id

  subject_alternative_names = length(var.domain_api) > 0 ? [var.domain_api, "profiler.${var.domain}"] : ["profiler.${var.domain}"]

  tags = var.tags
}