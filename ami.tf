data "aws_ami" "server" {
  most_recent = true

  owners = [var.aws_account_id]

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "is-public"
    values = ["false"]
  }

  filter {
    name   = "name"
    values = ["${var.app_name}-${var.app_version}-${var.php_version}-*","${var.app_name}-${var.app_version}-*"]
  }
}

